import React, { useState, useEffect, useRef } from "react";
import "./App.css";
import pidgeon from "./images/pigeon.png";

import { PIDGEON_ALPHABET, HUMAN_ALPHABET } from "./types";

function hasKey<O>(obj: O, key: keyof any): key is keyof O {
  return key in obj;
}

const App: React.FC = () => {
  const [text, setText] = useState("");
  const [translateOpt, setTranslateOpt] = useState("pt-pru");

  const inputArea = useRef<HTMLTextAreaElement>(null);

  useEffect(() => {
    if (inputArea.current) {
      inputArea.current.value = "";
    }
    setText("");
  }, [translateOpt]);

  const translateToPidgeon = (input: string) => {
    const listChar = input.split("");
    const translatedChars = listChar.map(e => {
      if (e === "," || e === "?" || e === "!" || e === "." || e === " ")
        return e;
      if (hasKey(PIDGEON_ALPHABET, e)) {
        return PIDGEON_ALPHABET[e];
      }
    });
    return translatedChars.join("");
  };

  const translateToHuman = (input: string) => {
    const listString = input.split(" ");
    const r = new RegExp("(P|p)(.+?)(U|Ú|Û|Ù|u|ú|ù|û){2}", "gm");
    const mappedToAlphabet = listString.map(s => {
      return s.match(r);
    });
    const translatedStrings = mappedToAlphabet.map(arr => {
      if (arr) {
        return arr
          .map(e => {
            if (hasKey(HUMAN_ALPHABET, e)) {
              return HUMAN_ALPHABET[e];
            }
          })
          .join("");
      }
      return "";
    });
    return listString
      .map((s, index) => {
        const r = new RegExp("^.*[^(!|?|,|.)$]", "g");
        return s.replace(r, translatedStrings[index]);
      })
      .join(" ");
  };

  return (
    <div className="App">
      <div className="cloud x1"></div>
      <div className="cloud x2"></div>
      <div className="cloud x3"></div>
      <div className="cloud x4"></div>
      <div className="cloud x5"></div>
      <div className="cloud x6"></div>
      <div className="cloud x7"></div>
      <div className="main_wrapper">
        <div className="title_wrapper">
          <img src={pidgeon} alt="" className="pidgeon-icon" />
          <h1>Pigeon Translator</h1>
        </div>
        <div className="translate-select">
          <select onChange={e => setTranslateOpt(e.target.value)}>
            <option value="pt-pru">Pt -> Pru</option>
            <option value="pru-pt">Pru -> Pt</option>
          </select>
        </div>
        <div className="input_wrapper">
          <textarea
            ref={inputArea}
            className="input-field"
            onChange={e => {
              if (translateOpt === "pt-pru") {
                setText(translateToPidgeon(e.target.value));
              } else {
                setText(translateToHuman(e.target.value));
              }
            }}
          />
        </div>
        <textarea className="text-field" defaultValue={text}></textarea>
      </div>
    </div>
  );
};

export default App;
